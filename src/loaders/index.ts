
import models from '../models';
import di from './di';

import cors from "cors";
import helmet from "helmet";

import routes from '../api';
import express from 'express';
import Logger from './logger';
import bodyParser from 'body-parser'

import morgan from 'morgan';
import swaggerUi from 'swagger-ui-express';
import path from 'path';
import dotenv from 'dotenv';
import { cronJob } from './cronjob';

export default async ({ expressApp }: any) => {


    await models.sequelize.authenticate()

    await di({
        modelInstance: models
    });

    Logger.info('Dependency Injector loaded');


    const documentPath = require(path.resolve('swagger.json'))
    expressApp.use('/api-docs', swaggerUi.serve, swaggerUi.setup(documentPath, { explorer: true }));



    expressApp.use(helmet());
    expressApp.use(cors());

    // for parsing application/json
    expressApp.use(express.json());

    expressApp.use(morgan('dev'));
    // log request 

    // Middleware that transforms the raw string of req.body into json
    // expressApp.use(bodyParser.json());

    // for parsing application/x-www-form-urlencoded
    expressApp.use(express.urlencoded({ extended: true }));
    // expressApp.use(bodyParser.urlencoded({ extended: true }))

    expressApp.use('/api', routes());

    expressApp.use('/public', express.static(path.resolve('public')));

    await cronJob('0 18 * * *', async () => {
        console.log("CronJob .... ")
    })
    Logger.info('Cronjob loaded');


    expressApp.use((req: any, res: any, next: (arg0: any) => void) => {
        const err: any = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    /// error handlers
    expressApp.use((err: any, req: any, res: any, next: any) => {
        /**
         * Handle 401 thrown by express-jwt library
         */
        if (err.name === 'UnauthorizedError') {
            return res
                .status(err.status)
                .send({ message: err.message })
                .end();
        }
        return next(err);
    });

    expressApp.use((err: any, req: any, res: any, next: any) => {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });
    });

}