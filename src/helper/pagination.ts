import { IPaginationQueryResult, IPagination } from "../interfaces/Pagination";
import { Request } from "express";
import paginate from 'express-paginate';


export const generatePaginationResult = (results: any, req: Request) => {
    const itemCount = results.count;
    const pageCount = Math.ceil(results.count / (req.query.limit as any));
    const pages = paginate.getArrayPages(req)(5, pageCount, (req.query.page as any));
    const hasNextPage = paginate.hasNextPages(req)(pageCount)
    return {
        currentPage: parseInt((req.query.page as any)),
        nextPage: hasNextPage ? ((parseInt((req as any).query.page) + 1) as number) : null,
        pageCount: pageCount,
        itemCount: itemCount,
        pages: pages,
        limit: req.query.limit,
    };
}

export const getPaginationQuery = (req: Request): IPagination => {
    // console.log("req page: ",req.query.page);
    // console.log("req query: ",req.query);
    return {
        limit: parseInt(req.query.limit as any,10),
        offset: (req.skip as any)
    }
}

export const getFilterQuery = (req: Request) => {
    if (req.query.sort_by) {
        const sortBy = (req.query.sort_by as string).split(',')
        const filters = sortBy.map((value) => {
            const sort = value.split(':')
            return [sort[0], sort[1] || 'asc']
        })
        return {
            order: filters || []
        }
    }
    return {}

}