import { Sequelize , QueryTypes } from "sequelize";

export default (sequelize: any, DataTypes:any) => {
    var Users = sequelize.define('Users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        mobile_no: {
            type: DataTypes.STRING
        },
        username: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING,
        },
        updated_at: {
            type: DataTypes.DATE,
        },
        created_at: {
            type: DataTypes.DATE,
        },
    }, {
        tableName: 'users',
        timestamps: true,
        underscored: true
    });

    // Sample.associate = (models:any ) => {
    //     models.ArmyArea.hasMany(models.User, { as: 'user', sourceKey: 'id', foreignKey: 'army_area_id' }),
    //         models.ArmyArea.hasMany(models.MilitaryCircle, { as: 'military_circle', sourceKey: 'id', foreignKey: 'area_id' })
    // };

    Users.getUsers = async (): Promise<any> => {
        const data = await Users.findAll({
            // where: { },
            // attributes: []
        });
        return data;
    }

    Users.getUser = async (username:string): Promise<any> => {
        const data = await Users.findOne({
            where: { username : username },
            attributes: ['id','mobile_no','username']
        });
        return data;
    }

    Users.getUserPassword = async (username:string): Promise<any> => {
        const data = await Users.findOne({
            where: { username : username },
            attributes: ['password']
        });
        return data;
    }

    Users.register = async (value:any) : Promise<any> =>{
        const data = await Users.create(value)
        return data ;
    }

    Users.updateUser = async (id:number,value:object) : Promise<any> =>{
        const data = await Users.update(value, {
            where: {
                id: id
            }
        })
        return data
    }


    


    return Users
}