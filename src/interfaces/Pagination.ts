export interface IPagination {
    offset: number;
    limit: number;
}

export interface IPaginationQueryResult {
    rows: number;
    count: number;
}

export interface IPaginationResponse {
    rows: any;
    pagination: any;
}