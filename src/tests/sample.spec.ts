
import axios from 'axios'

describe('Test Script', () => {
    let data = {}
    beforeEach( async () => { 
        const result = await axios.get('http://localhost:4988/api/sample/1')
        data = result.data.data
     });
    test('Test Sample', () => {
        expect({ name: "x" }).toEqual({ name: "x" })
    })

    test('Test Api toHaveProperty', async () => {
        expect(data).toHaveProperty('title')
    })

    test('Test toBeDefined', async () => {
        expect(data).toBeDefined()
    })
})