export interface IFile {
    fieldname: string;
    originalname: string;
    encoding: string;
    mimetype: string;
    destination: string;
    filename: string;
    path: string;
    size: number;
    type_media?:string;
}

export interface UsersInterface {
    id?:number
    mobile_no:string
    username: string
    password: string
    name?: string
}


export interface SampleRecordInterface {
    title: string
    short_description: string
    release_date: string
    developer: string
    thumbnail?: string
}

export interface SampleThumbnail {
    thumbnail:string
}

export interface SampleInterface extends SampleRecordInterface {
    id:number,
    createdAt:string,
    updatedAt:string
 }
