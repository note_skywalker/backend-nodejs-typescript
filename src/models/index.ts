import fs from 'fs';
import path from 'path';
import { Sequelize,DataTypes } from 'sequelize';

const basename = path.basename(__filename);
const db: any = {};

const databaseConfig: any = {
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    logging: console.log
}

const sequelize = new Sequelize(databaseConfig);

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename);
    })
    .forEach(file => {
        // console.log("FILE: ",path.join(__dirname, file))
        const modelPath = path.join(__dirname, file);
        const model = require(modelPath).default(sequelize, Sequelize);
        // console.log("model: ",model);
       // var model = require(`./${file}`)(sequelize, DataTypes); //require(path.join(__dirname, file))(sequelize, DataTypes) //sequelize['import'](path.join(__dirname, file));
        // const getKeyValue = <T extends object, U extends keyof T>(key: U) => (obj: T) => obj[key];
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;