import { Sequelize , QueryTypes, STRING } from "sequelize";
import { generatePaginationResult } from "../helper/pagination";
import { IPaginationResponse } from "../interfaces/Pagination";


export default (sequelize: any, DataTypes:any) => {
    var Sample = sequelize.define('Sample', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING
        },
        short_description: {
            type: DataTypes.STRING,
        },
        release_date: {
            type: DataTypes.STRING,
        },
        developer:{
            type: DataTypes.STRING,
        },
        thumbnail:{
            type:DataTypes.STRING
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        }
    }, {
        tableName: 'sample',
        timestamps: false,
        underscored: true
    });

    // Sample.associate = (models:any ) => {
    //     models.ArmyArea.hasMany(models.User, { as: 'user', sourceKey: 'id', foreignKey: 'army_area_id' }),
    // };

    Sample.getSample = async (paginationQuery:object,filterQuery:object,filter:object = {}) => {
        const rowsAsync = await Sample.findAll({
            where: filter ,
            // attributes: []
            ...paginationQuery,
            ...filterQuery
        });

        const countAsync = await Sample.count({
            where: filter,
            // ...paginationQuery
        })
        // const results = await Promise.all([rowsAsync, countAsync])
        // const rows = results[0] 
        return {
            result: rowsAsync,
            countResult: countAsync,
        };
    }

    Sample.getSampleById = async (id:number) =>{
        const data = await Sample.findOne({
            where : {'id':id}
        })

        return data
    }

    Sample.createSample = async (value:object) : Promise<any> =>{
        const data = await Sample.create(value)
        return data ;
    }

    Sample.createSampleBulk = async (value:any) : Promise<any> =>{
        const data = await Sample.bulkCreate(value)
        return data ;
    }

    Sample.updateSample = async (id:number,value:object) : Promise<any> =>{
        const data = await Sample.update(value, {
            where: {
                id: id
            }
        })
        return data
    }

    Sample.deleteSample = async (id:number) : Promise<any> =>{
        const data = await Sample.destroy({
            where: {
                id: id
            }
        })
        return data
    }

    Sample.deleteCron = async () : Promise<any>=>{
        const dattt = new Date();
        // const today = dattt.getFullYear()
        // const data = await Sample.delete(
        //     {
        //         where :{
        //             created_at : 111
        //         }
        //     }
        // )
    }

    return Sample
}