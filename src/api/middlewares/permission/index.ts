import { Request, Response, NextFunction } from "express";

// middleware for doing role-based permissions
export default function permit(methods: string | string[], ...allowed: any[]) {
    const isAllowed = (role: any) => allowed.indexOf(role) > -1;

    return (request: Request, response: Response, next: NextFunction) => {
        const method = request.method;
        if (methods.indexOf(method)< 0){
            return next(); 
        }

        if ((request as any).decoded.role && isAllowed((request as any).decoded.role))
            return next(); // role is allowed, so continue on the next middleware
        else {
            return response.status(403).json({ message: "Forbidden" }); // user is forbidden
        }
    }
}