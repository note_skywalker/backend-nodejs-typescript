const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLID, GraphQLList, GraphQLSchema } = graphql;

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        status: {
            type: GraphQLString,
            resolve(parent: any, args: any) {
                return "Welcome to GraphQL"
            }
        },
        nonthawat: {
            type: GraphQLString,
            resolve(parent: any, args: any) {
                return "Welcome to nonthawat GraphQL"
            }
        }
    }
});

const schema = new GraphQLSchema({
    query: RootQuery
});

export default schema
