import { Container } from 'typedi';
import Logger from './logger';

export default ({ modelInstance  } : any ) => {
  try {
    Container.set('models', modelInstance)
    Container.set('logger', Logger)
  } catch (e) {
    Logger.error('🔥 Error on dependency injector loader: %o', e);
    throw e;
  }
};
