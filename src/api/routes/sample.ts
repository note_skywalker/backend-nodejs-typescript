import { Router, Response, Request } from 'express';
import { Container } from 'typedi';
import {
    validationResults,
    SampleValidation
} from '../middlewares/validators';
import SampleService from '../../services/sample'
import { IPaginationResponse } from '../../interfaces/Pagination';
import { IFile } from '../../interfaces/IMain';

import multer from 'multer';
import fs from 'fs';
import path from 'path';
import randomstring from 'randomstring';

const axios = require('axios')

const router = Router();

router.get('/', async (req: Request, res: Response) => {
    try {
        const serviceInstance = Container.get(SampleService)
        const { rows, pagination }: IPaginationResponse = await serviceInstance.getSample(req)
        // const response = await serviceInstance.getSample();
        return res.status(200).json({
            "status": 200,
            // "data": response
            data: {
                sample: rows
            },
            pagination: pagination
        })
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }

})

router.get('/:sample_id',async (req:Request,res:Response)=>{
    try {
        const serviceInstance = Container.get(SampleService)
        const response = await serviceInstance.getSampleById(parseInt(req.params.sample_id))
        return res.status(200).json({
            "status": 200,
            "data": response
        })
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }
})

router.post('/createSample', SampleValidation.createSampleCheck, validationResults, async (req: Request, res: Response) => {
    try {
        const serviceInstance = Container.get(SampleService)
        const response = await serviceInstance.createSample(req.body);
        // console.log("body: ",req.body);
        return res.status(200).json({
            "status": 200,
            "data": response
        })
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }
})

router.patch('/updateSample/:sample_id', async (req: Request, res: Response) => {
    try {
        console.log("SAMPLE: ", req.params.sample_id);
        const serviceInstance = Container.get(SampleService)
        const response = await serviceInstance.updateSample(parseInt(req.params.sample_id), req.body);
        return res.status(200).json({
            "status": 200,
            "data": response
        })
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }
})

router.delete('/:sample_id',async (req:Request,res:Response)=>{
    try {
        const serviceInstance = Container.get(SampleService)
        const response = await serviceInstance.deleteSample(parseInt(req.params.sample_id))
        return res.status(200).json({
            "status": 200,
            "data": response
        })
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }
})

router.get('/getCron', async (req: Request, res: Response) => {
    const serviceInstance = Container.get(SampleService)
    const result = await axios({
        method: 'get',
        url: 'https://www.freetogame.com/api/games?platform=pc',
    })
    const dataQuery = result.data.slice(0, 15)
    const response = await serviceInstance.createSampleBulk(dataQuery);
    return res.status(200).json(response)
})

router.delete('/getDeleteCron', async (req: Request, res: Response) => {
    const serviceInstance = Container.get(SampleService)
    const response = await serviceInstance.deleteSampleBulk();
    return res.status(200).json(response)
})


router.get('/covid', async (req: Request, res: Response) => {
    const result = await axios({
        method: 'get',
        url: 'https://covid19.th-stat.com/json/covid19v2/getTodayCases.json'
    })
    return res.status(200).json(result.data)
})


const imagePrefixPath = ['public', 'upload']
const uploadStorage  = multer.diskStorage({
    destination: function (req: Request, file: any, cb) {
        const ThumbnailePath = path.resolve(imagePrefixPath.join('/'));
        (req as any).imagePrefixPath = imagePrefixPath.join('/');
        // console.log("ThumbnailePath: ",ThumbnailePath);
        if (!fs.existsSync(ThumbnailePath)) {
            fs.mkdirSync(ThumbnailePath)
        }

        cb(null, ThumbnailePath)
    },
    filename: function (req: Request, file, cb) {
        // console.log("FILE: ",file);
        // console.log("Req:", req.body);
        const file_name = file.originalname;
        let fileExtension = '.' + file_name.substr((file_name.lastIndexOf('.') + 1));
        if (fileExtension == '.content') {
            fileExtension = '.jpg';
        }
        const filename = Date.now() + '-' + randomstring.generate(7) + fileExtension //'.jpg'
        cb(null, filename)
    }
})

const uploadThumbnail = multer({ storage: uploadStorage })

router.patch('/:updateSample/:id/uploadThumbnail', async (req: Request, res: Response) => {

    uploadThumbnail.array('thumbnail')(req, res, async (err) => {
        // console.log("req.files: ", req.files);
        // console.log("imagePrefixPath: ", imagePrefixPath);
  
        if (err instanceof multer.MulterError) {
            // A Multer error occurred when uploading.
            return res.status(400).json({ message: err.message })
        } else if (err) {
            // An unknown error occurred when uploading.
            return res.status(400).json({ message: err.message })
        }

        try {
            // // await resizeImages((req as any).files)
            const serviceInstance = Container.get(SampleService)
            console.log("imagePrefixPath: ",imagePrefixPath.join('/'))
            let getFileData  = (req.files as  Array<IFile>)
            const thumbnailObj = {"thumbnail" :`${process.env.BACKEND_URL}${imagePrefixPath.join('/')}/${getFileData[0].filename}` } 
            // console.log(req.files.filename)
            const uploadThumbnailResponse = await serviceInstance.updateSample(parseInt(req.params.id),thumbnailObj)
            if(uploadThumbnailResponse){
                const response = {
                    status: 200,
                    data: uploadThumbnailResponse
                }
                return res.status(200).json(response)
            }
         
        } catch (error) {
            // logger.error(error)
            const response = {
                status: 400,
                message: error.message
            }
            return res.status(400).json(response)
        }
    })

})




export default router;