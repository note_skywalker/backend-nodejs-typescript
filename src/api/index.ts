import { Router} from 'express';
import AuthMiddleware from './middlewares/auth';
import permitMiddleware from './middlewares/permission';

import Pagination from 'express-paginate';

import auth from './routes/auth'
import sample from './routes/sample'

const router = Router();

// const roles = {
//     ADMIN: 'admin',
//     USER: 'user',
// }

export default () => {
    router.use(Pagination.middleware(10))
    router.use('/auth',auth);

    // router.use([
    //     "/manage/user/:userId"
    // ], AuthMiddleware, permitMiddleware(['GET','POST', 'PUT', 'PATCH'], roles.ADMIN))

    // router.use('/sample/createSample',AuthMiddleware,sample)
    // router.use('/sample/updateSample',AuthMiddleware,sample)
    router.use([
        '/sample/createSample',
        '/sample/updateSample'
    ],AuthMiddleware)

    router.use('/sample',sample);

    
    return router;
}