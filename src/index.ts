/**
 * Required External Modules
 */

import * as dotenv from "dotenv";
import express,{Response,Request} from "express";
import 'reflect-metadata';

import { graphqlHTTP } from 'express-graphql';
import schema from './screma/screma'
// import loaders from "./loaders";

dotenv.config();
/**
 * App Variables
 */

if (!process.env.PORT) {
   console.log("Couldn't find variable PORT in .env file")
   process.exit(1);
}

async function startServer() {
   const PORT: number = parseInt(process.env.PORT as string, 10);

   const app = express();
   app.use(
      "/graphql",
      graphqlHTTP({
         schema: schema,
         graphiql: true,
      }));

   app.get('/',(req:Request,res:Response)=>{
         return res.send('<h1> Api Work </h1>')
   })

   await require('./loaders').default({ expressApp: app })

   /**
    *  App Configuration
    */


   /**
    * Server Activation
    */


   app.listen(PORT, () => {
      console.log(`Listening on port ${PORT}`);
   });
}

startServer();