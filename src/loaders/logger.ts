import { createLogger, format, transports } from 'winston';
import path from 'path';

const filename = path.resolve('.log');

const transportLogs = [];
if (process.env.NODE_ENV !== 'development') {
    transportLogs.push(
        new transports.Console(),
        new transports.File({ filename })
    )
} else {
    transportLogs.push(
        new transports.Console()
    )

}

const logger = createLogger({
    level: 'silly',
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.errors({ stack: true }),
        format.colorize(),
        format.printf(
            info => `${info.timestamp} ${info.level}: ${info.message}`
        ),
        format.splat(),
        // format.json()
    ),
    transports: [new transports.Console()]
});

export default logger;