FROM node:10
WORKDIR /usr/src/app
COPY package.json ./
COPY ./build .
COPY ./public /usr/src/app/
COPY swagger.json ./
RUN npm install
RUN npm install pm2 -g
EXPOSE 4900

CMD ["pm2-runtime","index.js"]
