import axios from 'axios'
describe('Test Script Auth', () => {
    let data = {}
    beforeEach( async () => { 
        try {
            await axios.post('http://localhost:4988/api/auth/login',{username:'x',password:'xx'})
        } catch (error) {
            data = {
                "status":error.response.status,
                "message":error.response.data.message
            }
            // console.log("ERROR: ",error.response.data) 
        }
       
        // console.log("DATA: ",data);
     });
    test('Test Auth Login Fail', () => {
        expect(data).toHaveProperty("status",400)
        expect(data).toHaveProperty("message","Wrong")
    })
})