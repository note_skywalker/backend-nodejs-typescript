import { Router, Response, Request, NextFunction } from 'express';
import { verifyToken } from '../../../helper/jwt';
import logger from '../../../loaders/logger';

export default (req: Request, res: Response, next: NextFunction) => {
    const auth = req.headers["authorization"];
    if (!auth) {
        logger.error('Authentication Token is required.')
        return res.status(401).json({ message: 'Authentication Token is required.' })
    }
    try {
        const token = auth.split(" ")[1]
        const decoded: any = verifyToken(token);
        (req as any).decoded = decoded
        logger.info(`userId: ${(req as any).decoded.userData.id}`)
        try {
            // console.log("decoded: ",decoded)
            if (!decoded.userData.id) { // userId
                return res.status(401).json({ message: 'Authentication Token is not correct.' })
            }
        } catch (error) {
            return res.status(401).json({ message: 'Authentication Token is not correct [2].' })
        }

        next()
    } catch (error) {
        return res.status(401).json({ message: 'Authentication Token is failed.' })
    }
}