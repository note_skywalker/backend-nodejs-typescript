import jwt from 'jsonwebtoken';

export const generateAppToken = (inputData: any) => {
    try {
        const appToken = jwt.sign({userData:inputData}, `${process.env.JWT_SECRET}`, { expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRE });
        return appToken;
    } catch (error) {
        console.log("error:",error);
        throw new Error(error.message)
    }
}

export const verifyToken = (appToken: string) => {
    try {
        const decoded = jwt.verify(appToken, `${process.env.JWT_SECRET}`);
        return decoded;
    } catch (error) {
        throw new Error(error.message)
    }
}