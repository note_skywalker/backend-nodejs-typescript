import { Response, Request, NextFunction } from 'express';
import { check, validationResult, buildCheckFunction, body } from 'express-validator';

const bodyCheck = buildCheckFunction(['body'])
const queryCheck = buildCheckFunction(['query'])
const paramCheck = buildCheckFunction(['params'])

export const validationResults = async (req: Request, res: Response, next: NextFunction) => {
    const errors: any = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ message: errors.errors.map((item: any) => item.msg).join('. ') });
    }
    else {
        next();
    }

}

export namespace AuthValidation {
    export const loginCheck = [
        bodyCheck('username').exists().withMessage('username is not define'),
        bodyCheck('password').exists().withMessage('password is not define')
    ];
    export const mobileCheck = [
        bodyCheck('mobile_no').exists().withMessage('mobile error').isLength({ min: 10,max:10 }).withMessage('mobile length')
    ]
    export const otpCheck = [
        bodyCheck('mobile_no').exists().withMessage('mobile error').isLength({ min: 10,max:10 }).withMessage('mobile length'),
        bodyCheck('code').exists().withMessage('otp code error').isLength({ min: 6,max:6 }).withMessage('Otp length')
    ]
}


export namespace SampleValidation {
    export const createSampleCheck = [
        check('title')
            .exists().withMessage('Title is not define')
            .isLength({ min: 1 }).withMessage('Title must be at least 1 chars long'),
        check('short_description')
            .exists().withMessage('Description is not define')
    ];
    export const deleteSampleCheck = [
        paramCheck('id')
            .exists().withMessage('Sample Id is not define')
    ];
}