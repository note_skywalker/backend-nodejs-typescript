const CronJob = require('cron').CronJob;

export const cronJob = (time: any, onTick: any, onComplete = null) => {
    var job = new CronJob(time, onTick, onComplete, true, 'Asia/Bangkok');
    return job;
}