import { Request } from 'express'
import { Service, Inject } from 'typedi';
import { Logger } from 'winston';
import { generateAppToken, verifyToken } from '../helper/jwt';
import { UsersInterface } from '../interfaces/IMain'

const bcrypt = require('bcrypt');
const client = require('twilio')(process.env.TWILIO_ACCOUNTS_ID, process.env.TWILIO_AUTH_TOKEN);



@Service()
export default class AuthService {
    constructor(
        @Inject('models') private models: any,
        @Inject('logger') private logger: Logger
    ) {
        logger.info('AuthService Constructor')
    }


    async login(value: UsersInterface): Promise<any> {

        const result = await this.models.Users.getUserPassword(value.username)
        if(!result){
            throw new Error('Wrong');
        }
        const match = await bcrypt.compare(value.password, result.password);

        if (match) {
            const data = await this.models.Users.getUser(value.username)
            const appToken = generateAppToken(data);
            const {username , mobile_no} = data
            const userInfo = {username,mobile_no}
            // await this.models.Users.updateUser(data.id,{token:appToken})
            const tokenType = 'Bearer'

            return {appToken,tokenType,userInfo:userInfo}
        } else {
            throw new Error('Wrong');
        }

    }

    async register(value: UsersInterface): Promise<any> {
        const check = await this.models.Users.getUserPassword(value.username)
        if(!check){
            const saltRounds = 15;
            const password_hash = await bcrypt.hash(value.password, saltRounds)
            value.password = password_hash
            const result = await this.models.Users.register(value)
            return result
        }else{
            throw new Error('Dupicate !!');
        }
 

    }

    async sendOtp(mobileNo:string) {

        const modifyMobile = mobileNo.replace('0','+66')
        // console.log("modifyMobile: ",modifyMobile);
        try {
            const data = await client.verify.services(process.env.TWILIO_SERVICE_ID).verifications.create({
                to: `${modifyMobile}`,
                channel: 'sms'
            });
            if(data){
                return `Send to ${mobileNo} already` 
            }
            
        } catch (error) {

            this.logger.error(error.message);
            throw new Error(error.message);
        }

    }

    async verifyOtp(mobileNo:string, code:string) {

        const modifyMobile =  mobileNo.replace('0','+66') // mobileNo.replace(/^\0/, "+66")
        
        try {
            const data = await client.verify
                .services(process.env.TWILIO_SERVICE_ID)
                .verificationChecks
                .create({
                    to: `${modifyMobile}`,
                    code
                });
            if (!data.valid) {
                throw new Error("OTP Wrong!!")
                // return "OTP Wrong!!"
            } else {
                // return true
                const fakeValue = {"id":4988,"mobile_no":mobileNo ,"username":''}
                const appToken = generateAppToken(fakeValue);
                const {username , mobile_no} = fakeValue
                const userInfo = {username,mobile_no}
                // await this.models.Users.updateUser(data.id,{token:appToken})
                const tokenType = 'Bearer'
    
                return {appToken,tokenType,userInfo:userInfo}
            }
            // return data
        } catch (error) {

            this.logger.error(error.message);
            throw new Error(error.message);
        }
    }






}