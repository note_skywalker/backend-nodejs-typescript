import { Request } from 'express'
import { Service, Inject } from 'typedi';
import { Logger } from 'winston';

import { SampleRecordInterface, SampleInterface, SampleThumbnail } from '../interfaces/IMain'
import { generatePaginationResult, getPaginationQuery, getFilterQuery } from '../helper/pagination';
import { IPaginationResponse } from '../interfaces/Pagination';

import moment from 'moment';

@Service()
export default class SampleService {
    constructor(
        @Inject('models') private models: any,
        @Inject('logger') private logger: Logger
    ) {
        logger.info('SampleService Constructor')
    }

    getTest = async (id: number) => {
        return id
    }

    async getSample(req: Request): Promise<IPaginationResponse> {
        try {
            const paginationQuery = { ...getPaginationQuery(req) }
            const filterQuery = { ...getFilterQuery(req) }
            const { result, countResult } = await this.models.Sample.getSample(paginationQuery, filterQuery);
            console.log("countResult: ", countResult);
            const paginationResponse = generatePaginationResult({ count: countResult }, req)

            return {
                rows: result,
                pagination: paginationResponse,
            };
        } catch (error) {
            this.logger.error(error.message)
            throw new Error(error);
        }

    }

    async getSampleById(id: number) {
        const response = await this.models.Sample.getSampleById(id)
        return response
    }

    async createSample(data: SampleRecordInterface) {
        try {
            // ,"dd/mm/yy h:MM TT"
            const modifyFormat = moment(data.release_date).format('YYYY-MM-DD HH:mm:ss')
            data.release_date = modifyFormat
            const response = await this.models.Sample.createSample(data);
            return response
        } catch (error) {
            this.logger.error(error.message)
            throw new Error(error);
        }
    }

    async updateSample(sample_id: number, data: any ) {
        try {
            // console.log("UPDATE data: ", data);
            if(data.release_date){
                // onCreate and onUpdate only 
                const modifyFormat = moment(data.release_date).format('YYYY-MM-DD HH:mm:ss')
                data.release_date = modifyFormat
            }
            const response = await this.models.Sample.updateSample(sample_id, data);
            return response
        } catch (error) {
            this.logger.error(error.message)
            throw new Error(error);
        }
    }

    async deleteSample(sample_id: number) {
        try {
            // console.log("Delete data: ", sample_id);
            const response = await this.models.Sample.deleteSample(sample_id);
            return response
        } catch (error) {
            this.logger.error(error.message)
            throw new Error(error);
        }
    }

    async createSampleBulk(data: SampleRecordInterface) {
        try {
            const response = await this.models.Sample.createSampleBulk(data);
            return response
        } catch (error) {
            this.logger.error(error.message)
            throw new Error(error);
        }
    }

    async deleteSampleBulk() {
        try {
            const response = await this.models.Sample.deleteCron();
            return response
        } catch (error) {
            this.logger.error(error.message)
            throw new Error(error);
        }
    }

}