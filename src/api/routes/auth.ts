import { Router, Response, Request } from 'express';
import { Container,Inject } from 'typedi';
import AuthService from '../../services/auth';
import {
    validationResults,
    AuthValidation
} from '../middlewares/validators';

const router = Router();

router.post('/login', async (req: Request, res: Response) => {
    try {
        const authInstance = Container.get(AuthService)
        const result = await authInstance.login(req.body)
        const response = {
            status:200,
            data:result
        }
        return res.status(200).json(response)
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }

})


router.post('/register', async (req: Request, res: Response) => {
    try {
        const authInstance = Container.get(AuthService)
        const result = await authInstance.register(req.body)
        const response = {
            status:200,
            data:result
        }
        return res.status(200).json(response)
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }

})

router.post('/sendOtp',AuthValidation.mobileCheck, validationResults ,async (req:Request,res:Response)=>{
    try {
        const authInstance = Container.get(AuthService)
        const result = await authInstance.sendOtp(req.body.mobile_no) // +66 
        const response = {
            status:200,
            data:result
        }
        return res.status(200).json(response)
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }
}) 

router.post('/verifyOtp',AuthValidation.otpCheck, validationResults ,async (req:Request,res:Response)=>{
    try {
        const authInstance = Container.get(AuthService)
        const result = await authInstance.verifyOtp(req.body.mobile_no,req.body.code)
        const response = {
            status:200,
            data:result
        }
        return res.status(200).json(response)
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }
})

router.get('/getTest', async (req: Request, res: Response) => {
    try {
        const response = {
            status:200,
            data:2
        }
        return res.status(200).json(response)
    } catch (error) {
        const response = {
            status: 400,
            message: error.message
        }
        return res.status(400).json(response)
    }

})



export default router;